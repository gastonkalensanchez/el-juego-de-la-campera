using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerTemperaturaController : MonoBehaviour
{
    public Image incomodidad, temperatura;
    public float medidorTemperatura, medidorIncomodidad, end = 1;
    float temperaturaMax = 10f;
    float incomodidadMax = 5f;
    public bool tieneCampera, estaIncomodo;
    public bool _tieneCampera
    {
        get
        {
            return tieneCampera;
        }
        set
        {
            tieneCampera = value;
        }
    }
    public bool enLaSombra;
    private void Start()
    {
        temperaturaMax = 10f;
        incomodidadMax = 5f;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
            _tieneCampera = true;           
        if (Input.GetMouseButtonDown(1))
            _tieneCampera = false;
        incomodidad.fillAmount = medidorIncomodidad / incomodidadMax;
        temperatura.fillAmount = medidorTemperatura / temperaturaMax;
        TemperaturaController();
        if (GameManager.Instance.hacks)
        {
            if (Input.GetMouseButtonDown(0))
                GameManager.Instance.LlegoUbi();         
            if (Input.GetMouseButtonDown(1))
                GameManager.Instance.SetearNuevaUbicacion();
        }
        else
        {
            if (tieneCampera)
            {
                if (!enLaSombra)
                {
                    SubirTemperatura(true, 1);
                }
            }
            else
            {
                if (enLaSombra)
                {
                    SubirTemperatura(false, 2);
                }

                else
                {
                    SubirTemperatura(false, 1);
                }
            }
        }
        

    }
    
    public void TemperaturaController()
    {
        if (medidorTemperatura >= 10 || medidorTemperatura <= 0) medidorIncomodidad += Time.deltaTime;
        else
        {
            if (medidorIncomodidad >= 0)
            {
                if (tieneCampera)
                {

                        medidorIncomodidad += 0.5f * Time.deltaTime;
                   
                    
                }
                else
                {
                    medidorIncomodidad -= Time.deltaTime;
                }

                
            }
            else
            {
                medidorIncomodidad = 0;
            }
            
        }
        
        if (medidorIncomodidad >= 5f)
        { 
            estaIncomodo = true;
            medidorIncomodidad = 5;
        }
        else estaIncomodo = false;
        if (estaIncomodo) end -= 0.5f * Time.deltaTime;        
        else end = 1f;
        
    }
    public void SubirTemperatura(bool pSubir, int pMultiplicador)
    {
        if (medidorTemperatura < 0)
        {
            medidorTemperatura = 0;
        }
        if (medidorTemperatura >= 10)
        {
            medidorTemperatura = 10;
        }
        if (pSubir) medidorTemperatura += pMultiplicador * Time.deltaTime;
        else medidorTemperatura -= pMultiplicador * Time.deltaTime;
    }
}
