using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class ShadowController : MonoBehaviour
{
    public bool esta;
    void Update()
    {
        // Rayo hacia abajo desde la posici�n del objeto
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit))
        {
            // Verifica si el collider que golpe� est� configurado para recibir sombras
            if (hit.collider.gameObject.GetComponent<Renderer>().receiveShadows)
            {
                esta = true;
            }
            else
            {
                esta = false;
            }
        }
    }
}
    //public Light direccional;
    //private Renderer rend;
    //private Material material;
    //public float shadow;
    //private void Start()
    //{
    //    rend = GetComponent<Renderer>();
    //    material = rend.material;
    //}

    //private void Update()
    //{
    //    // Obtener la intensidad de luz recibida
    //    float lightAmount = GetLightAmount(transform.position);

    //    // Actualizar el color del material en funci�n de la intensidad de luz
    //    material.color = new Color(lightAmount, lightAmount, lightAmount);
    //    shadow = lightAmount;
    //}

    //private float GetLightAmount(Vector3 position)
    //{
    //    // Obtener la iluminaci�n en la posici�n especificada
    //    Light light = direccional; // Buscar la luz m�s cercana
    //    if (light != null)
    //    {
    //        // Calcular la cantidad de luz recibida de la luz m�s cercana

    //        float distanceToLight = Vector3.Distance(position, light.transform.position);
    //        float attenuation = 1.0f / (distanceToLight * distanceToLight); // Atenuaci�n cuadr�tica

    //        float lightIntensity = light.intensity;
    //        float lightAngle = Vector3.Angle(light.transform.forward, (position - light.transform.position).normalized);
    //        float cosAngle = Mathf.Cos(lightAngle * Mathf.Deg2Rad);

    //        float lightAmount = attenuation * lightIntensity * cosAngle;
    //        return lightAmount;
    //    }
    //    else
    //    {
    //        // No se encontr� ninguna luz
    //        return 0.0f;
    //    }
    //}

    //private Light FindClosestLight()
    //{
    //    // Buscar la luz m�s cercana al objeto

    //    Light closestLight = null;
    //    float closestDistance = Mathf.Infinity;

    //    foreach (Light light in FindObjectsOfType<Light>())
    //    {
    //        float distanceToLight = Vector3.Distance(transform.position, light.transform.position);
    //        if (distanceToLight < closestDistance)
    //        {
    //            closestDistance = distanceToLight;
    //            closestLight = light;
    //        }
    //    }

    //    return closestLight;
    //}


