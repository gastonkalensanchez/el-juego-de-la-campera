using System.Collections;
using System.Collections.Generic;
using TMPro;

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject start;
    public bool hacks;
    public GameObject camara,gameover;
    public Toggle togleCasa, togleUbi;
    public static GameManager Instance;
    public GameObject gym, casa, supermercado;
    public bool bGym, bSupermercado, bCasa;
    public TMP_Text causaPierd,dondeir;
    // Start is called before the first frame update
    void Start()
    {
        if (Instance == null)
            Instance = this;
        SetearNuevaUbicacion();
        Time.timeScale = 0;
        gameover.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (bGym)
        {
            gym.SetActive(true);
            dondeir.text = "Ir a el gimnasio";
        }
        else
        {
            gym.SetActive(false);
        }
        if (bCasa)
        {
            casa.SetActive(true);
   
        }
        else
        {
            casa.SetActive(false);
        }
        if (bSupermercado)
        {
            supermercado.SetActive(true);
            dondeir.text = "Ir a el supermercado";
        }
        else
        {
            supermercado.SetActive(false);
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            hacks = true;
        }
        else if (Input.GetKeyDown(KeyCode.P))
        {
            hacks = false;
        }
       
    }
    public void Play()
    {
        start.SetActive(false);
        Time.timeScale = 1;
    }
    public void Recargar()
    {
        SceneManager.LoadScene(0);
    }
    public void EndGame(string pDead)
    {
        Time.timeScale = 0;
        camara.gameObject.SetActive(false);
        gameover.SetActive(true);
        causaPierd.text = pDead;
        causaPierd.gameObject.SetActive(true);
    }
    public void SetearNuevaUbicacion()
    {
        togleUbi.isOn = false;
        togleCasa.gameObject.SetActive(false);
        bCasa = false;
        if (bGym)
        {
            bGym = false;
            bSupermercado = true;
        }
        else
        {
            bGym = true;
            bSupermercado = false;
        }
    }
    public void LlegoUbi()
    {
        togleUbi.isOn = true;
        bSupermercado = false;
        bGym = false;
        bCasa = true;
        togleCasa.gameObject.SetActive(true);
    }
}
