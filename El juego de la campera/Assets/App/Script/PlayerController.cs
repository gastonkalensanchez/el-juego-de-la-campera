using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public TMP_Text timeEnd;
    public GameObject campera;
    PlayerTemperaturaController playerTemperaturaController;
    PlayerMovement playerMovement;
    void Awake()
    {
        playerMovement = GetComponent<PlayerMovement>();
        playerTemperaturaController = GetComponent<PlayerTemperaturaController>();
    }
    void Start()
    {
        playerTemperaturaController.end = 1;
        playerTemperaturaController.medidorTemperatura = 5f;
    }
    void Update()
    {
        campera.SetActive(playerTemperaturaController.tieneCampera);
        if (playerTemperaturaController.estaIncomodo)
        {
            timeEnd.gameObject.SetActive(true);
            timeEnd.text = playerTemperaturaController.end.ToString("0.00");
        }
        else
        {
            timeEnd.gameObject.SetActive(false);
        }
        if (playerTemperaturaController.tieneCampera)
        {
            playerMovement.speed = 2.5f;
        }
        else
        {
            playerMovement.speed = 5f;
        }
        if (playerTemperaturaController.end <= 0)
        {
            playerTemperaturaController.end = 0;
            ENDGAME();
        }

    }
    public void ENDGAME()
    {

        if (playerTemperaturaController.medidorTemperatura >= 10)
        {
            GameManager.Instance.EndGame("TE DIO UN GOLPE DE CALOR");
        }
        else
        {
            GameManager.Instance.EndGame("TE DIO HIPOTERMIA");
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Casa")) 
        {
            GameManager.Instance.SetearNuevaUbicacion();
        }
        if (other.gameObject.CompareTag("Ubicacion"))
        {
            GameManager.Instance.LlegoUbi();

        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Sombra")) playerTemperaturaController.enLaSombra = true;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Sombra")) playerTemperaturaController.enLaSombra = false;
        
    }

}
