using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraController : MonoBehaviour
{
    float sensitivity = 5f;
    private Camera playerCamera;
    private Vector3 rotation = Vector3.zero;
    void Start()
    {
        playerCamera = GetComponentInChildren<Camera>();
    }
    private void FixedUpdate()
    {

        float mouseX = Input.GetAxis("Mouse X") * sensitivity;
        float mouseY = Input.GetAxis("Mouse Y") * sensitivity;
        rotation.x -= mouseY;
        rotation.x = Mathf.Clamp(rotation.x, -90f, 90f);
        rotation.y += mouseX;
        playerCamera.transform.localRotation = Quaternion.Euler(rotation.x, 0f, 0f);
        transform.eulerAngles = new Vector3(0f, rotation.y, 0f);
    }
}
